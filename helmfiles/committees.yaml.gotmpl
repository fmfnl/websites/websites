{{ readFile "templates/bases.yaml" }}

values:
  - namespace: committees
    volumeName: samba-committees

---

# Render YAML anchor/alias templates for later use in releases
templates:
  {{- tpl (readFile "templates/releases/website.yaml.gotmpl") . | nindent 2 }}
  {{- tpl (readFile "templates/releases/volume.yaml.gotmpl") . | nindent 2 }}

# Template of a standard committee website, rendering to string to use as a template later on
{{- $site_template := tpl (readFile "committees/website.yaml.gotmpl") . }}

releases:
  - <<: *volume
    name: {{ .Values.volumeName }}
    values:
      - path: /home/commissies

  # Make releases from website definition file
  {{- range (readFile "committees/websites.yaml" | fromYaml).websites }}
    {{- tpl $site_template . | nindent 2 }}
  {{- end }}

  - <<: *website
    name: fotocie-forward
    values:
      - app:
          image: {{ .Values.registryBase }}/server-php:7.2
          mounts:
            {{ .Values.volumeName }}:
              - mountPoint: /fmf
                path: Fotocie/public_html2
        ingress:
          dns:
            hostnames:
              - fotocie.{{ .Values.domain }}
          hostnames:
            - host: fotocie.{{ .Values.domain }}
            {{- if (eq .Values.environment "production") }}
            - host: fotocie.nl
            {{- end }}

  - <<: *website
    name: fotocie-old
    values:
      - app:
          image: {{ .Values.registryBase }}/server-phpperl:5.6-imagemagick
          mounts:
            {{ .Values.volumeName }}:
              - mountPoint: /fmf
                path: Fotocie/public_html
              - mountPoint: /schijf
                path: Fotocie/fotocie-schijf
        ingress:
          dns:
            hostnames:
              - old.fotocie.{{ .Values.domain }}
          hostnames:
            - host: old.fotocie.{{ .Values.domain }}
            {{- if (eq .Values.environment "production") }}
            - host: old.fotocie.nl
            {{- end }}
